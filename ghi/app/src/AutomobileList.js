import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function AutomobilesList() {
    const [automobiles, setAutomobiles] = useState([]);
    const getAutomobiles = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            const automobiles = data.autos;
            setAutomobiles(automobiles);
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        getAutomobiles();
    }, [])

    return (
        <div>
            <Link className="btn btn-warning m-3" to="new/">Add Automobile</Link>
            <table className="table table-striped">
                <thead>
                    <tr className="text-white">
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map((automobile) => {
                        return (
                            <tr key={automobile.href} className="text-white">
                                <td className="text-white">{automobile.vin}</td>
                                <td className="text-white">{automobile.color}</td>
                                <td className="text-white">{automobile.year}</td>
                                <td className="text-white">{automobile.model.name}</td>
                                <td className="text-white">{automobile.model.manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default AutomobilesList
