import { NavLink } from 'react-router-dom';
import jeep from './images/jeep.png';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light" style={{backgroundColor: "#16fff5"}}>
      <div className="container-fluid navbar-center">
        <NavLink className="navbar-brand" to="/">
        <img src={jeep} style={{width: "130px", height: "80px",}}/></NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mb-2 mb-lg-0 fs-3 text-left">
            <li className="nav-item text-black mx-5 px-5">
              <NavLink className="nav-link text-black" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li>
              <NavLink className="nav-link text-black mx-5 px-5" to="/models">Vehicle Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link text-black mx-5 px-5" to="/automobiles">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link text-black mx-5 px-5" to="/appointments">Service Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link text-black mx-5 px-5" to="/sales">Sales</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
