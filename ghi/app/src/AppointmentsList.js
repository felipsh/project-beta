import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function AppointmentsList() {
    const [appointments, setAppointment] = useState([]);

    const getAppointment = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            const appointments = data.appointments;
            let unfinishedAppointment = []
            for (let appointment of appointments) {
                if (appointment.finished === false) {
                    unfinishedAppointment.push(appointment)
                }
            }
            setAppointment(unfinishedAppointment);
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        getAppointment();
    }, [])

    async function handleFinish(id) {
        const url = `http://localhost:8080/api/appointments/${id}`;
        let data = {
            "finished": true
        }
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(url, fetchConfig);
        return getAppointment()


    }

    async function handleDelete(id) {
        const url = `http://localhost:8080/api/appointments/${id}`;
        const fetchConfig = {
            method: "DELETE",
        }

        const response = await fetch(url, fetchConfig)
        return getAppointment()
    }

    return (
        <div>
            <Link className="btn btn-warning m-3" to="technicians/new/">Add Technician</Link>
            <Link className="btn btn-warning m-3" to="new/">Create Appointment</Link>
            <Link className="btn btn-warning m-3" to="history/">Find Appointment</Link>
            <table className="table table-striped m-3">
                <thead>
                    <tr className="text-white">
                        <th></th>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody className="text-white">
                    {appointments.map((appointment) => {
                        let vip = ""
                        if (appointment.vip) {
                            vip = "VIP"
                        }
                        const dateObject = new Date(appointment.datetime);
                        const date = dateObject.toLocaleDateString()
                        const time = dateObject.toLocaleTimeString([], { hour: "2-digit", minute: "2-digit" })
                        return (
                            <tr key={appointment.id} id={appointment.id} className="text-white">
                                <td className='text-danger text-center'>
                                    {vip}
                                </td>
                                <td className="text-white">{appointment.vin}</td>
                                <td className="text-white">{appointment.name}</td>
                                <td className="text-white">{date}</td>
                                <td className="text-white">{time}</td>
                                <td className="text-white">{appointment.technician.name}</td>
                                <td className="text-white">{appointment.reason}</td>
                                <td>
                                    <div className="btn-group text-white" role="group">
                                        <button type="button" className='btn btn-danger' onClick={() => handleDelete(appointment.id)}>Cancel</button>
                                        <button type="button" className='btn btn-success' onClick={() => handleFinish(appointment.id)}>Finished</button>
                                    </div>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default AppointmentsList
